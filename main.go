package main

import (
	"errors"
	"fmt"
)

const (
	LeftIntervalIndex  = 0
	RightIntervalIndex = 1
)

type RangeList struct {
	data [][2]int
}

func (rangeList *RangeList) Add(rangeElement [2]int) error {
	if rangeList.data == nil {
		rangeList.data = make([][2]int, 0)
	}

	inputLeft := rangeElement[LeftIntervalIndex]
	inputRight := rangeElement[RightIntervalIndex]

	for index, val := range rangeList.data {
		left, right := val[LeftIntervalIndex], val[RightIntervalIndex]
		// 完全落在某个区间范围内，无需任何变更
		if inputLeft >= left && inputRight <= right {
			return nil
		}
		// 左区间在某个区间范围内，右区间在某个区间范围外，该区间的右区间需要更改
		if inputLeft >= left && inputLeft <= right {
			if inputRight >= right {
				rangeList.data[index][RightIntervalIndex] = inputRight
				return nil
			}
		}
	}
	rangeList.data = append(rangeList.data, rangeElement)
	return nil
}

func (rangeList *RangeList) Remove(rangeElement [2]int) error {
	if rangeList.data == nil {
		return errors.New("data is empty")
	}

	inputLeft := rangeElement[LeftIntervalIndex]
	inputRight := rangeElement[RightIntervalIndex]

	var affectRanges []int
	for index, val := range rangeList.data {
		left, right := val[LeftIntervalIndex], val[RightIntervalIndex]
		// 完全在某个范围的左区间内，无需变更
		if inputRight == left && inputLeft == left {
			return nil
		}

		// 在某个方位内左区间开始删除元素，变更左区间
		if inputLeft == left && inputRight < right {
			rangeList.data[index][LeftIntervalIndex] = inputRight
			return nil
		}

		// 完全在某个区间范围内,该范围需要割裂
		if inputLeft > left && inputRight < right {
			// 当前范围右区间需要变更
			oldRight := rangeList.data[index][RightIntervalIndex]
			rangeList.data[index][RightIntervalIndex] = inputLeft
			// 增加一个范围, 范围左区间为输入的右区间，范围右区间为原来范围的右区间
			rangeList.data = append(rangeList.data, [2]int{inputRight, oldRight})
			return nil
		}

		// 跨范围
		affectRanges = append(affectRanges, index)
	}

	// 当前情况存在跨范围删除, data只需要保留头尾两个范围, 并且分别变更他们的右区间和左区间
	affectRangesLen := len(affectRanges)
	if affectRangesLen >= 2 {
		head := affectRanges[0]
		tail := affectRanges[len(affectRanges)-1]
		rangeList.data[head][RightIntervalIndex] = inputLeft
		rangeList.data[tail][LeftIntervalIndex] = inputRight
		// 删除中间的范围切片
		midStartIndex := affectRanges[1]
		midIndexNumber := affectRangesLen-2
		rangeList.data = append(rangeList.data[:midStartIndex], rangeList.data[midStartIndex+midIndexNumber:]...)
	}

	return nil
}

func (rangeList *RangeList) Print() error {
	if rangeList.data == nil {
		return nil
	}
	var text string
	for _, item := range rangeList.data {
		text += fmt.Sprintf("[%d,%d) ", item[0], item[1])
	}
	fmt.Println(text)
	return nil
}

func main() {
	rl := &RangeList{}
	rl.Add([2]int{1, 5})
	rl.Print()
	rl.Add([2]int{10, 20})
	rl.Print()
	rl.Add([2]int{20, 21})
	rl.Print()
	rl.Add([2]int{2, 4})
	rl.Print()
	rl.Add([2]int{3, 8})
	rl.Print()
	rl.Remove([2]int{10, 10})
	rl.Print()
	rl.Remove([2]int{10, 11})
	rl.Print()
	rl.Remove([2]int{15, 17})
	rl.Print()
	rl.Remove([2]int{3, 19})
	rl.Print()
}
